<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComparisonFieldsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('comparison_criteria')->nullable();
            $table->boolean('comparison_1_enabled')->nullable();
            $table->boolean('comparison_2_enabled')->nullable();
            $table->boolean('comparison_3_enabled')->nullable();
            $table->string('comparison_1_image')->nullable();
            $table->string('comparison_2_image')->nullable();
            $table->string('comparison_3_image')->nullable();
            $table->string('comparison_1_title')->nullable();
            $table->string('comparison_2_title')->nullable();
            $table->string('comparison_3_title')->nullable();
            $table->text('comparison_0_values')->nullable();
            $table->text('comparison_1_values')->nullable();
            $table->text('comparison_2_values')->nullable();
            $table->text('comparison_3_values')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('comparison_criteria');
            $table->dropColumn('comparison_1_enabled');
            $table->dropColumn('comparison_2_enabled');
            $table->dropColumn('comparison_3_enabled');
            $table->dropColumn('comparison_1_image');
            $table->dropColumn('comparison_2_image');
            $table->dropColumn('comparison_3_image');
            $table->dropColumn('comparison_1_title');
            $table->dropColumn('comparison_2_title');
            $table->dropColumn('comparison_3_title');
            $table->dropColumn('comparison_0_values');
            $table->dropColumn('comparison_1_values');
            $table->dropColumn('comparison_2_values');
            $table->dropColumn('comparison_3_values');
        });
    }
}
