@push ('scripts')
    @if ($errors->hasAny(['footer_contact_name', 'footer_contact_email', 'footer_contact_phone']))
        <script>
            swal({
                title: "Contact Error!",
                text: "{{ $errors->all()[0] }}",
                icon: "error",
                buttons: ["Cancel", "Go to contact from"],
            }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#footer").offset().top
                }, 500);
            });
        </script>
    @endif
@endpush

<!--Footer-->
<footer id="footer" class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="widget widget-tag">
                        <h3 class="widget-title">Name Menu</h3>
                        <ul class="widget-list">
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li><a href="{{ url('/about') }}">About Us</a>
                            </li>
                            <li><a href="{{ url('/products')}}">Products</a>
                            </li>
                            <li><a href="{{ url('/blog')}}">Blog</a>
                            </li>
                            <li><a href="{{ url('/contacts') }}">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="widget widget-services">
                        <h3 class="widget-title">Products</h3>
                        <ul class="widget-list">
                            @each('components.top-menu-item', \App\Category::all() ?? [], 'category')
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-6">
                    <div class="widget widget-services">
                        <div class="contact-person">
                            <img src="{{ Voyager::image('contact-person.jpg') }}" alt="contact-person">
                        </div>
                        <div class="contact login footer-form">
                            <h3 class="widget-title">Have a Question?</h3>
                            <!--/.heading-title-->
                            <form class="contact-form clearfix" method="post" action="{{ url(route('contact_attempts.footer')) }}">
                                @csrf
                                <div class="your-name">
                                    <input class="input-field" placeholder="Name*" required type="text" name="footer_contact_name" value="{{ old('footer_contact_name') }}" id="footer_contact_name">
                                </div>
                                <div class="email">
                                    <input class="input-field" placeholder="E-Mail*" required type="email" name="footer_contact_email" value="{{ old('footer_contact_email') }}" id="footer_contact_email">
                                </div>
                                <div class="email">
                                    <input class="input-field" placeholder="Phone*" required type="tel" name="footer_contact_phone" value="{{ old('footer_contact_phone') }}" id="footer_contact_phone">
                                </div>
                                <div class="your-message">
                                    <textarea name="footer_contact_message" placeholder="Write your message" cols="10" rows="6" id="footer_contact_message">{{ old('footer_contact_message') }}</textarea>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn-submit btn-hover">Submit</button>
                                </div>
                            </form>
                            <!--/.contant-form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/.footer-->
