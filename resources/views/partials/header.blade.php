<!--Header-->
<div class="header">
    <!--Header-Top-->
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="left"> Welcome to United Distributuon <span><i class="fa fa-phone"></i></span> +1 773 376 9660 </div>
                    <!--/.left-->
                </div>
                <!--/.col-md-6-->
                <div class="col-md-6 col-sm-6 col-xs-6">

                    <!--/.right-->
                </div>
                <!--/.col-md-6-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-top-->
    <!--Header-Middle-->
    <div class="header-middle">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8 col-sm-8 col-xs-12 site-logo">
                    <div class="logo">
                        <a href="{{ url('/') }}"><img src="{{ Voyager::Image(setting('site.logo')) }}" alt="logo" />
                        </a>
                    </div>
                </div>
                <!--/.site-logo-->
                <div class="col-md-4 col-sm-4 col-xs-12 header-search">
                    <div class="search default">
                        <form class="searchform" action="{{ url(route('search')) }}" method="get">
                            <div class="input-group">
                                <input type="search" id="dsearch" name="s" class="form-control" placeholder="Search for something..."> <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" id="submit-btn"><span class="arrow_right"></span>
                                    </button>
                                    </span>
                            </div>
                        </form>
                    </div>
                    <!--/.search-->
                </div>
                <!--/.header-search-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-middle-->
    <!--Navbar-->
    <div class="navbar groham-nav megamenu">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('/about') }}">About Us</a></li>
                    <li class="dropdown megamenu-fw">
                        <a href="{{ url('/products') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        @if (true)
                                            <ul>
                                                <li><a href="{{ url('/products') }}">All Products</a></li>
                                                @each('components.top-menu-item', \App\Category::all()->split(2)[0] ?? [], 'category')
                                            </ul>
                                        @endif
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        @if (true)
                                            <ul>
                                                @each('components.top-menu-item', \App\Category::all()->split(2)[1] ?? [], 'category')
                                            </ul>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-sm-6 menuimg"> <img src="{{ asset('/images/blog_listed1.jpg') }}" alt="" class="img-responsive"> </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/blog')}}">Blog</a>
                    <li><a href="{{ url('/contacts') }}">Contact us</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!--/.navbar-->
</div>
<!--/.header-->
