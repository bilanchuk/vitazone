<?php
/**
 * @var \App\Page $page
 */
?>
@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ $route ?? $page->link }}">Technical Data Sheets</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->
            <div class="about01">
                <!-- about shop area -->
                <div class="about_our_shop_area">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="about_shop_content">
                                <h3>{{ $page->title }}</h3>
                                {!! $page->body !!}
                                <div class="single-post-page">
                                    <ul class="documentations mlb30">
                                        @foreach (\App\Product::all() as $product)
                                            @if(!empty($files = (array) json_decode($product->files)))
                                                <h3>{{ $product->title }}</h3>
                                                @foreach ((array) json_decode($product->files) as $file)
                                                    <li><a href="/storage/{{ $file->download_link }}" target="_blank">{{ $file->original_name }}</a></li>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </ul>
                                    <!--/.Documentation's area-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.about shop area-->
        </div>
        <!--/.container-->
        </div>
    </div>
    <!--/.about-content-->
@endsection
