@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@push ('scripts')
    <!--Jquery ui-->
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <!-- google map -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>
    <!-- settings -->
    <script type="text/javascript" src="{{ asset('js/contact.map.js') }}"></script>

    @if ($errors->hasAny(['contact_name', 'contact_email', 'contact_phone']))
        <script>
            swal({
                title: "Contact Error!",
                text: "{{ $errors->all()[0] }}",
                icon: "error",
                buttons: ["Cancel", "Go to contact from"],
            }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#contact_form").offset().top
                }, 500);
            });
        </script>
    @endif
@endpush

@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ url('/contacts')}}">contact us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <!--contact-page-->
            <div class="content contact-page">
                <div class="row">
                    <!--Groham-Supports-->
                    <div class="groham-contact clearfix">
                        <div class="col-md-4 col-sm-6">
                            <div class="single-support">
                                <div class="support-img">
                                    <img alt="icon" src="/images/contact-icon1.png">
                                </div>
                                <div class="hover-support-img">
                                    <img alt="icon" src="/images/contact-icon-1.png">
                                </div>
                                <div class="support-text">
                                    <h3>phone number</h3>
                                    <p>+1-773-376-9660 / +1-773-376-9661</p>
                                </div>
                            </div>
                            <!--/.single-support-->
                        </div>
                        <!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="single-support">
                                <div class="support-img">
                                    <img alt="icon" src="/images/contact-icon-211.png">
                                </div>
                                <div class="hover-support-img">
                                    <img alt="icon" src="/images/contact-iconhover-2.png">
                                </div>
                                <div class="support-text">
                                    <h3>e-mail address</h3>
                                    <p>info@everestoil.template.com</p>
                                </div>
                            </div>
                            <!--/.single-support-->
                        </div>
                        <!--/.col-md-4-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-support">
                                <div class="support-img">
                                    <img alt="icon" src="/images/contact-icon3.png">
                                </div>
                                <div class="hover-support-img">
                                    <img alt="icon" src="/images/contact-icon-3.png">
                                </div>
                                <div class="support-text">
                                    <h3>location address</h3>
                                    <p>9101 West Fullerton Ave, office 1A Franklin Park,</p>
                                    <p>IL 60131 United States</p>
                                </div>
                            </div>
                            <!--/.single-support-->
                        </div>
                        <!--/.col-md-4-->
                    </div>
                    <!--/.groham-supports-->
                    <div id="contact_form" class="col-md-12">
                        <div class="contact login">
                            <div class="heading-title">
                                <h3 class="reply-title">leave a reply</h3>
                            </div>
                            <!--/.heading-title-->
                            <form class="contact-form clearfix" method="post" action="{{ url(route('contact_attempts.contact')) }}">
                                @csrf
                                <div class="col-md-4 col-sm-6">
                                    <div class="your-name">
                                        <label for="your-name">Name <span class="required">*</span>
                                        </label>
                                        <br>
                                        <input class="input-field" type="text" name="contact_name" value="{{ old('contact_name') }}" id="contact_name">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-4 col-sm-6">
                                    <div class="email">
                                        <label for="your-email">Email <span class="required">*</span>
                                        </label>
                                        <br>
                                        <input class="input-field" type="email" name="contact_email" value="{{ old('contact_email') }}" id="contact_email">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-4 col-sm-6">
                                    <div class="phone-number">
                                        <label for="phone-number">Phone Number <span class="required">*</span>
                                        </label>
                                        <input class="input-field" type="text" name="contact_phone" value="{{ old('contact_phone') }}" placeholder="" id="contact_phone">
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-12">
                                    <div class="your-message">
                                        <label for="your-message">Your Message</label>
                                        <br>
                                        <textarea name="contact_message" cols="10" rows="6" id="contact_message">{{ old('contact_message') }}</textarea>
                                    </div>
                                </div>
                                <!--/.col-md-3-->

                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn-submit btn-hover">Submit</button>
                                </div>
                            </form>
                            <!--/.contant-form-->
                        </div>
                        <!--/.contant-->
                    </div>
                    <!--/.col-md-12-->
                    <div class="col-md-12" style="margin-top: 3rem">
                        <div class="g-map full">
                            <div id='gmap_canvas'></div>
                        </div>
                    </div>
                    <!--/.col-md-12-->
                </div>
                <!--end of row-->
            </div>
            <!--/contact-pate-->
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->
@endsection
