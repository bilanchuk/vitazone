<?php
/**
 * @var \App\Category $category
 * @var \App\Product $products
 */
?>
@extends('layouts.master')
@section('title', $page->seo_title )
@section('meta_keyword', $page->meta_keywords )
@section('meta_description', $page->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content shop-grid">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">{{ __('Home') }}</a>
                            </li>
                            <li class="active"><a
                                        href="{{ $category->link ?? url('/products') }}">{{ $category->title ?? __('All Products') }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <div class="row">
                <div class="col-md-12">
                    <div class="product-listing-view">
                        <div class="view-navigation">
                            <div class="info-text">

                                <?php $count=0; ?>
                                @if (isset($category))
                                    @foreach ($category->products as $item)
                                    @if ($item->active==1)
                                        <?php $count++; ?>
                                    @endif
                                @endforeach
                                <p>Showing 1 - {{$count}}</p>
                                @else
                                    @foreach ($products as $item)
                                    @if ($item->active==1)
                                        <?php $count++; ?>
                                    @endif
                                @endforeach
                                <p>Showing {{$count?1:0}} - {{$count}}</p>
                                @endif

                            </div>
                            <div class="right-content">
                                <div class="grid-list">
                                </div>
                                <div class="input-select">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="shop-product-list">
                                @each('components.products-2', $category->products ?? $products ?? [], 'product', 'components.empty-products')
                            </div>
                        </div>
                    </div>
                    <!--content -->
                    <div class="content">
                        {!! $page->body !!}
                    </div>
                    <!-- /content -->

                    {{--<!-- pagination -->
                    <div class="paginations text-center">
                        <ul class="pagination">
                            <li><a href="#"><span class="pagicon arrow_carrot-left"></span></a>
                            </li>
                            <li><a href="#"><span>1</span></a>
                            </li>
                            <li class="active"><a href="#"><span>2</span></a>
                            </li>
                            <li><a href="#"><span>3</span></a>
                            </li>
                            <li><a href="#"><span>4</span></a>
                            </li>
                            <li><a href="#"><span>5</span></a>
                            </li>
                            <li><a href="#"><span>6</span></a>
                            </li>
                            <li><a href="#"><span class="pagicon arrow_carrot-right"></span></a>
                            </li>
                        </ul>
                    </div>
                    <!--/.pagination-->
--}}
                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->
@endsection
