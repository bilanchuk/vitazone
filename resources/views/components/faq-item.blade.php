<?php
/**
 * @var \App\Faq $faq
 */
?>
<h2>{{ $faq->question }}</h2>
<p>
    {{ $faq->answer }}
</p>
