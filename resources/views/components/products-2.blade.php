@if ($product->active==1)
    <div class="col-md-3 col-sm-6 col-xs-6 product_item">
        <div class="product-single">
            <a href="{{ $product->link }}">
                <div class="product-thumb">
                    @if ($product->image)
                        <img class="img-responsive" alt="Single product"
                            src="{{ Voyager::image($product->image) }}">
                    @endif
                </div>
                <!--/.product-thumb-->
                <div class="product-info">
                    <h2 class="price"> {{ $product->title }} </h2>
                    <h2 style="margin-top: 0.5rem">{{ implode(', ', $product->categories->pluck('title')->toArray()) }}</h2>
                </div>
            </a>
            <!--/.product-info-->
        </div>
        <!--/.product-single-->
    </div>
    <!--/.col-md-3-->
@endif

