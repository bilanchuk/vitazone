<div class="col-md-3 col-sm-6 col-xs-6 product_item">
    <a href="{{ $latest_solution->link }}" class="product-single">
        <div class="product-thumb">
            <img class="img-responsive" alt="Single product" src="{{ Voyager::image($latest_solution->image) }}">
        </div>
        <!--/.product-thumb-->
        <div class="product-info">
            <h2 class="price"> {{ $latest_solution->title }} </h2>
            <h2 style="margin-top: 0.5rem">{{ implode(', ', $latest_solution->categories->pluck('title')->toArray()) }}</h2>
        </div>
        <!--/.product-info-->
    </a>
    <!--/.product-single-->
</div>
