<div class="media single_recent_post clearfix">
    <a class="pull-left" href="#"><img class="media-object img-responsive" src="{{ Voyager::image($article->image) }}" alt="" />
    </a>
    <div class="media-body">
        <a href="{{ $article->link }}"><h3 class="media-heading">{{ $article->title }}</h3></a>
        <p style="max-height: 50px;">{{ $article->excerpt }}</p>
    </div>
</div>
