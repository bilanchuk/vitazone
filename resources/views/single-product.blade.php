@extends('layouts.master')
@section('title', $product->seo_title)
@section('meta_keyword', $product->meta_keywords)
@section('meta_description', $product->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ url('/products') }}">Products</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->
            <div class="single-produce-page no-sidebar">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="single-product-details clearfix">
                            <div class="col-md-5">
                                <div class="single-slider-item">
                                    @if (count($product->all_images))
                                        @if (count($product->all_images) > 1)
                                            <ul class="owl-slider">
                                                @foreach ((array) $product->all_images as $image)
                                                    <li class="item">
                                                        <img src="{{ Voyager::image($image) }}" alt="" class="img-responsive">
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <ul class="thumbnails-wrapper" id="thumbnails-wrapper">
                                                @foreach ($product->all_images as $image)
                                                    <li class="thumbnails">
                                                        <a href="#"><img src="{{ Voyager::image($image) }}" alt="" class="img-responsive">
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @else
                                            <img src="{{ Voyager::image($product->image) }}" alt="{{ $product->title }}" class="img-responsive">
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="right-content">
                                    <div class="product-info">
                                        <h2>{{ $product->title }}</h2>
                                        <div class="product-description">
                                            <h5 class="small-title">DESCRIPTION</h5>
                                            {!! $product->excerpt !!}
                                        </div>
                                        <div class="item-size">
                                            <h5 class="small-title">Available packaging:</h5>
                                            <div class="size-content-custom">
                                                @foreach ($product->packagings as $packaging)
                                                    <label><span>{{ $packaging->name }}</span></label>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="product-desc">
                                            <span class="item-number"><b>Product Number:</b>  #{{ $product->product_id }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="product-tab">
                                <ul class="nav nav-tabs">
                                    <li class="nav active"><a data-toggle="tab" href="#tab1">PRODUCT DESCRIPTION</a>
                                    </li>
                                    <li><a data-toggle="tab" href="#tab2">Documentations</a>
                                    </li>
                                    <li><a data-toggle="tab" href="#tab3">Compare with other brands</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active">
                                        {!! $product->description !!}
                                    </div>
                                    <div id="tab2" class="tab-pane">
                                        {{--@dd(json_decode($product->files))--}}
                                        <div class="single-post-page">
                                            <ul class="documentations">
                                                @foreach ((array) json_decode($product->files) as $file)
                                                    <li><a href="/storage/{{ $file->download_link }}" target="_blank"><i class="fa fa-file fa-2x"></i> {{ $file->original_name }}</a></li>
                                                @endforeach
                                            </ul>
                                            <!--/.Documentation's area-->
                                        </div>
                                    </div>
                                    <div id="tab3" class="tab-pane">
                                        <div class="related-product-content">
                                            @if ($product->show_comparison_table)
                                                <div class="comparison">

                                                    <table>
                                                        <thead>
                                                        <tr>
                                                            <th class="tl tl2"></th>
                                                            <th class="qbse">
                                                                Everest
                                                            </th>
                                                            <th colspan="3" class="qbo">
                                                                Similar products of other brands
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="tl"></th>
                                                            @foreach ($product->comparison_titles as $comparison_title)
                                                                <th class="compare-heading">{{ $comparison_title }}</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th></th>
                                                            @foreach ($product->comparison_images as $comparison_image)
                                                                <th class="price-info"><img
                                                                            src="{{ Voyager::image($comparison_image) }}"
                                                                            alt=""></th>
                                                            @endforeach
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($product->comparison_rows as $comparison_row)
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="4">{{ $comparison_row['title'] }}</td>
                                                            </tr>
                                                            <tr class="compare-row">
                                                                <td>{{ $comparison_row['title'] }}</td>
                                                                @foreach ($comparison_row['values'] as $comparison_row_value)
                                                                    <td>{{ $comparison_row_value }}</td>
                                                                @endforeach
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="related-products">
                                <div class="heading-title">
                                    <h3 class="title-text">related products</h3>
                                </div>
                                <div class="related-product-content">
                                    @foreach(\App\Product::orderBy(DB::raw('RAND()'))->take(4)->get()->all() as $product)
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="product-single">
                                            <a href="{{ $product->link }}">
                                            <div class="product-thumb">
                                                @if ($product->image)
                                                    <img class="img-responsive" alt="Single product" src="{{ Voyager::image($product->image) }}">
                                                @endif
                                            </div>
                                            <!--/.product-thumb-->
                                            <div class="product-info">
                                                <h2>{{ $product->title }}</h2>
                                            </div>
                                            </a>
                                            <!--/.product-info-->
                                        </div>
                                        <!--/.product-single-->
                                    </div>
                                    <!--/.col-md-4-->
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->
@endsection
