<?php
/**
 * @var \App\Page $page
 */
?>
@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ $route ?? $page->link }}">About Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->
            <div class="about01">
                <!-- about shop area -->
                <div class="about_our_shop_area">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
							@if($page->image)
								<img class="img-responsive" src="{{ Voyager::image($page->image) }}" alt="about everestoil" />
							@endif
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="about_shop_content">
                                <h3>{{ $page->title }}</h3>
                                {!! $page->body !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.about shop area-->
        </div>
        <!--/.container-->
        </div>
    </div>
    <!--/.about-content-->
@endsection
