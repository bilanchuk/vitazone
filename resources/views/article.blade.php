@extends('layouts.master')
@section('title', $article->seo_title)
@section('meta_keyword', $article->meta_keywords)
@section('meta_description', $article->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ route('blogpost') }}">blog post</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->


            <div class="single-post-page">
                <div class="row">
                    <div class="col-md-9">
                        <article class="post">
                            <div class="post-thumb-content">
                                <figure class="post-thumb">
                                    <a href="#">
                                        <img class="img-responsive" alt="thumb" src="/images/blog/blog-thumb51.jpg">
                                    </a>
                                </figure>
                                <!--/.post thumb-->
                                <span class="entry-date">15th apr. 2018</span>
                            </div>
                            <!--/.post-thumb-content-->
                            <div class="post-details">
                                <h3 class="entry-title">
                                    Lorem Ipsum Dolor
                                </h3>
                                <!--/.entry title-->
                                <div class="entry-content">
                                    <p>
                                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                    </p>

                                    <p>
                                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                                    </p>

                                    <p>
                                        Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <!--/.entry content-->
                            </div>
                            <!--/.post details-->
                            <div class="share-post">
                                <span>Share</span>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                            <!--/.author social-->
                        </article>
                        <!--/.post-->
                    </div>
                    <!--/.col-md-10-->
                </div>
                <!--/.row-->
            </div>
            <!--/.site-content-->
        </div>
        <!--/.container-->
    </div>
    <!--/.site-content-->
@endsection