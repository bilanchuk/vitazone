function init_map() {
    var myOptions = {
        zoom: 15,
        center: new google.maps.LatLng(41.921631, -87.85312699999997),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('gmap_canvas'),
        myOptions);
    marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(41.921631, -87.85312699999997)
    });
    infowindow = new google.maps.InfoWindow({
        content: '<strong>9101 Fullerton Ave, Franklin Park, IL 60131, USA</strong><br><br>'
    });
    google.maps.event.addListener(marker, 'click',
        function() {
            infowindow.open(map, marker);
        });
    infowindow.open(map, marker);
}
google.maps.event.addDomListener(window, 'load', init_map);
