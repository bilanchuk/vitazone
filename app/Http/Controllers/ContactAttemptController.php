<?php

namespace App\Http\Controllers;

use App\ContactAttempt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactSend;

class ContactAttemptController extends Controller
{
    public function footer(Request $request)
    {
        $request->validate([
            'footer_contact_name' => 'required|max:255',
            'footer_contact_email' => 'required|max:255|email',
            'footer_contact_phone' => 'required|max:255',
        ]);
        $name = $request->input('footer_contact_name');
        $email = $request->input('footer_contact_email');
        $phone = $request->input('footer_contact_phone');
        $message = $request->input('footer_contact_message');
        try {
            $contact_attempt = new ContactAttempt();
            $contact_attempt->name = $name;
            $contact_attempt->email = $email;
            $contact_attempt->phone = $phone;
            $contact_attempt->message = $message;
            $contact_attempt->save();
            
            $toEmail = 'info@everestoil.template.com';
            
            $feedback = [
                'name' => $name,
                'email' => $email, 
                'phone' => $phone,
                'message' => $message,
            ];
            Mail::to($toEmail)->send(new ContactSend($feedback));
        } catch (\Exception $e) {
            return \Redirect::back()->with('swal_status', json_encode([
                'title' => 'Contact attempt failed.',
                'text' => 'Please contact us manually.',
                'icon' => 'error',
            ]));
        }

        return \Redirect::back()->with('swal_status', json_encode([
            'title' => 'We\'ve got your message.',
            'text' => 'We wil return with response as soon as possible!',
            'icon' => 'success',
        ]));
    }

    public function contact(Request $request)
    {
        $request->validate([
            'contact_name' => 'required|max:255',
            'contact_email' => 'required|max:255|email',
            'contact_phone' => 'required|max:255',
        ]);
        $name = $request->input('contact_name');
        $email = $request->input('contact_email');
        $phone = $request->input('contact_phone');
        $message = $request->input('contact_message');
        try {
            $contact_attempt = new ContactAttempt();
            $contact_attempt->name = $name;
            $contact_attempt->email = $email;
            $contact_attempt->phone = $phone;
            $contact_attempt->message = $message;
            $contact_attempt->save();
            
            $toEmail = 'info@everestoil.template.com';
            
            $feedback = [
                'name' => $name,
                'email' => $email, 
                'phone' => $phone,
                'message' => $message,
            ];
            Mail::to($toEmail)->send(new ContactSend($feedback));
        } catch (\Exception $e) {
            return \Redirect::back()->with('swal_status', json_encode([
                'title' => 'Contact attempt failed.',
                'text' => 'Please contact us manually.',
                'icon' => 'error',
            ]));
        }

        return \Redirect::back()->with('swal_status', json_encode([
            'title' => 'We\'ve got your message.',
            'text' => 'We wil return with response as soon as possible!',
            'icon' => 'success',
        ]));
    }

    

    public function order_marketing_materials(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $message = $request->input('text');
        //try {
            $contact_attempt = new ContactAttempt();
            $contact_attempt->name = $name;
            $contact_attempt->email = $email;
            $contact_attempt->phone = $phone;
            $contact_attempt->message = $message;
            $contact_attempt->save();
            
            $toEmail = 'info@everestoil.template.com';
            
            $feedback = [
                'name' => $name,
                'email' => $email, 
                'phone' => $phone,
                'message' => $message,
            ];
            Mail::to($toEmail)->send(new ContactSend($feedback));
        //} catch (\Exception $e) {
            return \Redirect::back()->with('swal_status', json_encode([
                'title' => 'Contact attempt failed.',
                'text' => 'Please contact us manually.',
                'icon' => 'error',
            ]));
        //}

        return \Redirect::back()->with('swal_status', json_encode([
            'title' => 'We\'ve got your message.',
            'text' => 'We wil return with response as soon as possible!',
            'icon' => 'success',
        ]));
    }
}
