<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Packaging
 *
 * @property int $id
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Packaging whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Packaging whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Packaging whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Packaging whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Packaging whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Packaging extends Model
{
    //
}
